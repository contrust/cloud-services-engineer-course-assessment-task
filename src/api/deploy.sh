#!/bin/sh

./increment_version.sh version.txt
version=$(cat version.txt)

docker buildx build -t ${API_REPOSITORY_NAME}:${version} .
docker push ${API_REPOSITORY_NAME}:${version}

yc sls container revisions deploy \
  --folder-id ${FOLDER_ID} \
  --service-account-id ${SERVICE_ACCOUNT_ID} \
  --image ${API_REPOSITORY_NAME}:${version} \
  --container-id ${API_CONTAINER_ID} \
  --memory 512M \
  --cores 1 \
  --execution-timeout 5s \
  --concurrency 2 \
  --environment VERSION=${version},DOCUMENT_API_ENDPOINT=${DOCUMENT_API_ENDPOINT},AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID},AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY} 
