from pydantic import BaseModel


class Message(BaseModel):
    id: str
    sender: str
    message: str
