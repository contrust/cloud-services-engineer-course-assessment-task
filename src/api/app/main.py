import uvicorn
from fastapi import FastAPI

from YDBClient import YDBclient
from config import Config


app = FastAPI()
config = Config()
ydb_client = YDBclient()


@app.get("/info")
async def get_info():
    return {"version": config.version, 'replica_name': ydb_client.get_replica_name()}


@app.post("/message")
async def post_message(sender: str, message: str):
    saved_message = ydb_client.save_message(sender, message)
    return {'message': saved_message}

@app.get('/messages')
async def get_messages():
    messages = ydb_client.get_messages()
    return {'messages': messages}


if __name__ == '__main__':
    uvicorn.run(app, host='127.0.0.1', port=8080)
