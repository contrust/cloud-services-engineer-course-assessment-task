from pydantic import BaseSettings


class Config(BaseSettings):
    version: str = '0.0.1'
    aws_access_key_id: str
    aws_secret_access_key: str
    region: str = 'ru-central1-a'
    document_api_endpoint: str
