from uuid import uuid4

import boto3
from config import Config
from models import Message


class YDBclient:
    def __init__(self):
        self.config = Config()
        self.ydb_docapi_client = boto3.resource('dynamodb',
                                                endpoint_url=self.config.document_api_endpoint,
                                                region_name=self.config.region,
                                                aws_access_key_id=self.config.aws_access_key_id,
                                                aws_secret_access_key=self.config.aws_secret_access_key)

    def create_tables(self):
        messages_table = self.ydb_docapi_client.create_table(
            TableName='messages',
            KeySchema=[
                {
                    'AttributeName': 'id',
                    'KeyType': 'HASH'
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'id',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'sender',
                    'AttributeType': 'S'
                },
                {
                    'AttributeName': 'message',
                    'AttributeType': 'S'
                }
            ]
        )
        replicas_table = self.ydb_docapi_client.create_table(
            TableName='replicas',
            KeySchema=[
                {
                    'AttributeName': 'id',
                    'KeyType': 'HASH'
                },
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'id',
                    'AttributeType': 'N'
                },
                {
                    'AttributeName': 'name',
                    'AttributeType': 'N'
                }
            ]
        )
        self.ydb_docapi_client.Table('replicas').put_item(Item={'id': 1, 'count': 0})
        return (messages_table, replicas_table)

    def save_message(self, sender: str, message: str):
        table = self.ydb_docapi_client.Table('messages')
        message_id = str(uuid4())
        table.put_item(Item={
            'id': message_id,
            'sender': sender,
            'message': message
        })
        return message_id

    def get_messages(self):
        table = self.ydb_docapi_client.Table('messages')
        response = table.scan()
        data = response['Items']

        while 'LastEvaluatedKey' in response:
            response = table.scan(ExclusiveStartKey=response['LastEvaluatedKey'])
            data.extend(response['Items'])

        return data

    def get_replica_name(self):
        inc = self.ydb_docapi_client.Table('replicas').update_item(
            Key={
                'id': 1
            },
            UpdateExpression='ADD count :val',
            ExpressionAttributeValues={":val": 1},
            ReturnValues="UPDATED_NEW"
        )
        return inc['Attributes']['count']
