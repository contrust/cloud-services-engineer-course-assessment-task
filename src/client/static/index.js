const apiDomain = "";

const messagesForm = document.getElementById('message-form');
const backendVersionValue = document.getElementById('backend-version-value');
const frontendVersionValue = document.getElementById('frontend-version-value');
const replicaNameValue = document.getElementById('replica-name-value');
const messages = document.getElementById('messages');

async function fetchBackendChanges() {
	const messagesData = await fetch(`${apiDomain}/messages`)
				.then(response => response.json())
				.catch(error => console.log(error));
	console.log(messagesData)
        messages.innerHTML = messagesData["messages"].map(
		message => `<div class="message">
                	<div>Sender : ${message["sender"]}</div>
                	<div>Message : ${message["message"]}</div>
            		    </div>`)
		.join('');
	const backendData = await fetch(`${apiDomain}/info`)
                                .then(response => response.json())
                                .catch(error => console.log(error));
	backendVersionValue.innerHTML = backendData['version'];
	replicaNameValue.innerHTML = backendData['replica_name'];
}


frontendVersionValue.innerHTML = await fetch('version.cfg')
		.then(response => response.text())
		.catch(error => console.log(error));

messagesForm.addEventListener('submit', async (event) => {
	event.preventDefault();

	const formData = new FormData(messagesForm);
	await fetch(`${apiDomain}/message?sender=${formData.get('sender')}&message=${formData.get('message')}`, {
           method: 'POST',
           headers: {'Accept': 'application/json',}
        }).then(data => console.log(data))
          .catch(error => console.log(error));
	await fetchBackendChanges();
});

await fetchBackendChanges();
