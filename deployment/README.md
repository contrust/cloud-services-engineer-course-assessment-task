## О проекте
Просто гостевая книжка, в которой можно получать сообщения всех пользователей и отправлять свои сообщения.
## Ссылка
https://d5dfris6cclbeloi9epu.apigw.yandexcloud.net
## Инфраструктура
Все переменные окружения нужно задавать через export.
```sh
make init
make give_permissions_to_all_executables
```
Затем нужно заполнить пустые поля в provider.tf, а также задать переменную окружения FOLDER_ID.
```sh
make create_service_account
```
После этого надо задать переменную окружения SERVICE_ACCOUNT_ID, полученную из предыдущего шага.
```sh
make add_access_bindings
make create_access_key
```
После создания ключа, нужно задать его параметры в storage_bucket.tf, в переменных окружения AWS_ACCESS_KEY_ID и AWS_SECRET_ACCESS_KEY и в AWS-CLI.
```sh
aws configure
make apply_all_but_gateways
```
Далее нужно задать переменные окружения WEBSITE_BUCKET_NAME, API_REPOSITORY_NAME, API_CONTAINER_ID (не задана явно, это id только что созданного контейнера) и DOCUMENT_API_ENDPOINT, а также подставить все значения в фигурных скобках на настоящие в api.yaml и website.yaml.
```sh
make apply_gateways
make create_tables
```
Потом нужно подставить значение переменной API_GATEWAY_DOMAIN из вывода в переменную apiDomain из ../src/client/static/index.js.

Значение WEBSITE_GATEWAY_DOMAIN из вывода- адрес сайта.

## Деплой
Backend
```sh
make deploy_api
```
Frontend
```sh
make deploy_client
```
Backend && Frontend
```sh
make deploy
```
