locals {
  website_gateway_name = "website-gateway2312"
}

resource "yandex_api_gateway" "website-gateway" {
  name      = local.website_gateway_name
  spec      = file("website.yaml")
  folder_id = local.folder_id
}

output "WEBSITE_GATEWAY_DOMAIN" {
  value = "https://${yandex_api_gateway.website-gateway.domain}"
}
