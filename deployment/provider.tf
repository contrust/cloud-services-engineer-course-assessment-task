locals {
  token      = ""
  cloud_id   = ""
  folder_id  = ""
  zone       = "ru-central1-a"
}

provider "yandex" {
  cloud_id  = local.cloud_id
  folder_id = local.folder_id
  token     = local.token
  zone      = local.zone
}

terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}
