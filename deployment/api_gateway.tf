resource "yandex_api_gateway" "api-gateway" {
  name      = "guestbook-gateway4123"
  folder_id = local.folder_id
  spec      = file("api.yaml")
}

output "API_GATEWAY_DOMAIN" {
  value = "https://${yandex_api_gateway.api-gateway.domain}"
}
