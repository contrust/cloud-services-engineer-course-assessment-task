locals {
  service_account_name = "guestbook-service-account"
}

resource "yandex_iam_service_account" "service_account" {
  name        =  local.service_account_name
}

output "SERVICE_ACCOUNT_ID" {
  value = yandex_iam_service_account.service_account.id
}
