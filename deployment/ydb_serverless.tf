locals {
  database_name = "guestbook-database"
}

resource "yandex_ydb_database_serverless" "guestbook-database" {
  name      = local.database_name
  folder_id = local.folder_id
}

output "DOCUMENT_API_ENDPOINT" {
  value = yandex_ydb_database_serverless.guestbook-database.document_api_endpoint
}

output "DATABASE_PATH" {
  value = yandex_ydb_database_serverless.guestbook-database.database_path
}
