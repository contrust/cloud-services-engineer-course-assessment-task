locals {
    registry_name = "guestbook"
    repository_name = "guestbook_api"
}

resource "yandex_container_registry" "registry" {
  name      = local.registry_name
  folder_id = local.folder_id
}

resource "yandex_container_repository" "api" {
  name = "${yandex_container_registry.registry.id}/${local.repository_name}"
}


output "API_REPOSITORY_NAME" {
  value = "cr.yandex/${yandex_container_repository.api.name}"
}
