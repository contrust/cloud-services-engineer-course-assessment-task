locals {
  website_bucket_name = "website-bucket423423"
}

resource "yandex_storage_bucket" "website-bucket" {
  bucket     = local.website_bucket_name
  access_key = ""
  secret_key = ""
}

output "WEBSITE_BUCKET_NAME" {
  value = yandex_storage_bucket.website-bucket.bucket
}
